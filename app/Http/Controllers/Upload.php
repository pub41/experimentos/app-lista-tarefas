<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Upload extends Controller
{

    public function uploadFile (Request $request)
    {
        $extension_allow = ['jpg', 'jpeg'];

        if (!in_array($request->file('logo')->extension(), $extension_allow)) {
            return "Arquivo não permitido"; 
        }

        $image = Storage::put('uploads', $request->file('logo'));
        return str_replace("uploads/", "", $image);
    }

    public function getImage ($imageName = null)
    {
        return asset('uploads/'.$imageName);
    }
}
