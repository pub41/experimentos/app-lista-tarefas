<?php

use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "Hello app-lista-tarefas";
});

Route::post('/upload', '\App\Http\Controllers\Upload@uploadFile');

Route::get('/getImage/{nome?}', '\App\Http\Controllers\Upload@getImage');

Route::get('/vue', function () {
    return View('welcome');
});

Route::get('/vue2', function () {
    return View('welcome2');
});
